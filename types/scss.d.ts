declare module '*.scss' {
    interface ISassFile {
        [className: string]: string;
    }

    const scssFile: ISassFile;

    export = scssFile;
}
