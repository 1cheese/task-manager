export enum ButtonStyle {
    GRAY = 'gray',
    BLUE = 'blue',
    GREEN = 'green',
    BLUE_WITHOUT_BG = 'blueWithoutBG',
}
