import * as React from 'react';

import * as css from './Button.scss';
import { ButtonStyle } from '../enums/ButtonStyle';
import { classList } from '../../MainModule/utils/classList';

type ButtonProps = React.DetailedHTMLProps<
    React.ButtonHTMLAttributes<HTMLButtonElement>,
    HTMLButtonElement
> & {
    buttonStyle?: ButtonStyle;
};

const Button: React.FC<ButtonProps> = ({
    buttonStyle = ButtonStyle.GRAY,
    children,
    ...props
}) => (
    <button
        {...props}
        className={`${classList({
            [css.component]: true,
            [css.gray]: buttonStyle === ButtonStyle.GRAY,
            [css.blue]: buttonStyle === ButtonStyle.BLUE,
            [css.green]: buttonStyle === ButtonStyle.GREEN,
            [css.blueWithoutBG]: buttonStyle === ButtonStyle.BLUE_WITHOUT_BG,
        })} ${props.className ?? ''}`}
    >
        {children}
    </button>
);

export { Button };
