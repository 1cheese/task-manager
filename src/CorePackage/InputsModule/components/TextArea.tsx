import * as React from 'react';

import * as css from './TextArea.scss';

type TextAreaProps = React.DetailedHTMLProps<
    React.InputHTMLAttributes<HTMLTextAreaElement>,
    HTMLTextAreaElement
>;

const TextArea: React.FC<TextAreaProps> = ({ value, ...props }) => (
    <textarea
        {...props}
        value={value ?? ''}
        className={`${css.textarea} ${props.className ?? ''}`}
    />
);

export { TextArea };
