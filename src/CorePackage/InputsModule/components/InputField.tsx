import * as React from 'react';

import * as css from './InputField.scss';

type InputFieldProp = {
    inputId?: string;
    label?: string;
};

const InputField: React.FC<InputFieldProp> = ({ inputId, label, children }) => (
    <div className={css.component}>
        {label && (
            <label htmlFor={inputId} className={css.label}>
                {label}
            </label>
        )}
        <div className={css.inputContainer}>{children}</div>
    </div>
);

export { InputField, InputFieldProp };
