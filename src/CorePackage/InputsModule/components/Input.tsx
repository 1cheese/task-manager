import * as React from 'react';

import * as css from './Input.scss';

type InputProps = React.DetailedHTMLProps<
    React.InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
>;

const Input: React.FC<InputProps> = ({ type = 'text', value, ...props }) => (
    <input
        {...props}
        type={type}
        value={value ?? ''}
        className={`${css.input} ${props.className ?? ''}`}
    />
);

export { Input };
