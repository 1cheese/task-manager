import * as React from 'react';
import { format as dateFnsFormat, parse as dateFnsParse } from 'date-fns';
import { DateUtils } from 'react-day-picker';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import { DayPickerInputProps } from 'react-day-picker/types/Props';
import 'react-day-picker/lib/style.css';

import * as css from './DatepickerInput.scss';
import { CalendarIcon } from '../../IconsModule/components/CalendarIcon';
import { DATE_FORMAT } from '../../MainModule/constants/DATE_FORMAT';

export type DatepickerInputProps = Pick<
    DayPickerInputProps,
    'value' | 'onDayChange'
>;

function parseDate(str: string, format: string): Date | undefined {
    const parsed = dateFnsParse(str, format, new Date());

    if (DateUtils.isDate(parsed)) {
        return parsed;
    }

    return undefined;
}

function formatDate(date: Date, format: string): string {
    return dateFnsFormat(date, format);
}

const DatepickerInput: React.FC<DatepickerInputProps> = props => {
    return (
        <div className={css.component}>
            <DayPickerInput
                {...props}
                classNames={{
                    container: css.container,
                    overlayWrapper: css.overlayWrapper,
                    overlay: css.overlay,
                }}
                dayPickerProps={{
                    weekdaysShort: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
                }}
                format={DATE_FORMAT}
                formatDate={formatDate}
                parseDate={parseDate}
                placeholder={DATE_FORMAT}
            />
            <div className={css.icon}>
                <CalendarIcon />
            </div>
        </div>
    );
};

export { DatepickerInput };
