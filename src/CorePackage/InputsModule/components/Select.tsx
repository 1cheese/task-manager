import * as React from 'react';

import * as css from './Select.scss';
import { ArrowDownIcon } from '../../IconsModule/components/ArrowDownIcon';

type SelectProps = React.DetailedHTMLProps<
    React.SelectHTMLAttributes<HTMLSelectElement>,
    HTMLSelectElement
> & {
    options: Pick<HTMLOptionElement, 'disabled' | 'value' | 'label'>[];
};

const Select: React.FC<SelectProps> = ({
    options,
    value,
    className,
    ...props
}) => (
    <div className={`${css.component} ${className ?? ''}`}>
        <select {...props} value={value ?? ''} className={css.select}>
            <option value={''}>{props.placeholder}</option>
            {options.map(({ disabled, value: optionValue, label }, index) => (
                <option disabled={disabled} value={optionValue} key={index}>
                    {label}
                </option>
            ))}
        </select>
        <div className={css.icon}>
            <ArrowDownIcon />
        </div>
    </div>
);

export { Select, SelectProps };
