import { RouteComponentProps } from 'react-router';

export interface IAppRoute<P> {
    props: RouteComponentProps<P>;
}
