import * as React from 'react';

export const ArrowDownIcon: React.FC = () => (
    <svg
        width="10"
        height="6"
        viewBox="0 0 10 6"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
    >
        <path
            d="M4.29295 5.70699L0.292996 1.707C0.0976648 1.51166 0 1.25589 0 0.999998C0 0.744109 0.0976648 0.488332 0.292996 0.292999C0.683326 -0.0976664 1.31676 -0.0976664 1.70709 0.292999L4.99994 3.58588L8.29291 0.292999C8.68324 -0.0976664 9.31667 -0.0976664 9.707 0.292999C10.0977 0.683665 10.0977 1.31644 9.707 1.70711L5.70705 5.7071C5.31672 6.09765 4.68317 6.09765 4.29295 5.70699Z"
            fill="#2F4858"
        />
    </svg>
);
