import * as React from 'react';

export const ClockIcon: React.FC = () => (
    <svg
        width="14"
        height="14"
        viewBox="0 0 14 14"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
    >
        <path
            d="M7 13C10.3137 13 13 10.3137 13 7C13 3.68629 10.3137 1 7 1C3.68629 1 1 3.68629 1 7C1 10.3137 3.68629 13 7 13Z"
            stroke="#4E525D"
        />
        <path
            d="M6.91003 3.17676V7.17676H9.5767"
            stroke="#4E525D"
            strokeLinecap="round"
        />
    </svg>
);
