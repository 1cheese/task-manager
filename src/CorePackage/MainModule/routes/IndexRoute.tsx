import { AbstractRoute, route } from '../../RouteModule';

@route()
export class IndexRoute extends AbstractRoute {
    public static url() {
        return '/';
    }
}
