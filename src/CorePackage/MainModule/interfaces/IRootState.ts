import { IAppState } from './IAppState';
import { ITasksListState } from '../../../TasksPackage/TasksListModule/interfaces/ITasksListState';
import { ICreateAndEditTaskState } from '../../../TasksPackage/CreateAndEditModule/interfaces/ICreateAndEditTaskState';

export interface IRootState {
    App: IAppState;
    TasksList: ITasksListState;
    CreateAndEditTask: ICreateAndEditTaskState;
}
