export interface IUserDTO {
    id: string;
    first_name: string;
    last_name: string;
}
