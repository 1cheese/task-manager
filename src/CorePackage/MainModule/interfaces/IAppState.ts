import { IUser } from './IUser';

export interface IAppState {
    isFetching: boolean;
    users: IUser[];
}
