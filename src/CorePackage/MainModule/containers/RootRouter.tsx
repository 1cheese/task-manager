import * as React from 'react';
import { Switch } from 'react-router-dom';

import { MainLayout } from '../components/MainLayout';
import { TasksListRouter } from '../../../TasksPackage/TasksListModule/routers/TasksListRouter';
import { IndexRouter } from '../router/IndexRouter';
import { CreateTaskRouter } from '../../../TasksPackage/CreateAndEditModule/routers/CreateTaskRouter';
import { EditTaskRouter } from '../../../TasksPackage/CreateAndEditModule/routers/EditTaskRouter';

const RootRouter: React.FC = () => {
    return (
        <MainLayout>
            <Switch>
                <IndexRouter />

                <TasksListRouter />
                <CreateTaskRouter />
                <EditTaskRouter />
            </Switch>
        </MainLayout>
    );
};

export { RootRouter };
