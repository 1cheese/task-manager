import reduceReducers from 'reduce-reducers';

import { IAppState } from '../interfaces/IAppState';
import { defaultState } from '../utils/defaultState';
import { fetchUsersReducer } from './fetchUsersReducer';

export const appStateReducer = reduceReducers<IAppState, any>(
    defaultState ?? null,
    fetchUsersReducer,
);
