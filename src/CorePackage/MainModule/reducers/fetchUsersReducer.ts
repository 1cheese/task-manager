import { Reducer } from 'redux';

import { IAppState } from '../interfaces/IAppState';
import { defaultState } from '../utils/defaultState';
import {
    FetchUsersAction,
    FetchUsersPendingAction,
    FetchUsersRejectedAction,
    FetchUsersFulfilledAction,
} from '../actions/FetchUsersAction';

type TAction =
    | FetchUsersAction
    | FetchUsersPendingAction
    | FetchUsersRejectedAction
    | FetchUsersFulfilledAction;

export const fetchUsersReducer: Reducer<IAppState, TAction> = (
    state = defaultState,
    action,
): IAppState => {
    switch (action.type) {
        case FetchUsersPendingAction.type:
            return {
                ...state,
                isFetching: true,
            };

        case FetchUsersRejectedAction.type:
            return {
                ...state,
                isFetching: false,
            };

        case FetchUsersFulfilledAction.type:
            return {
                ...state,
                isFetching: false,
                users: action.payload,
            };

        default:
            return state;
    }
};
