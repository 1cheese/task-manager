import * as React from 'react';

import css from './MainLayout.scss';
import { Sidebar } from './Sidebar';
import { Header } from './Header';

const MainLayout: React.FC = ({ children }) => (
    <div className={css.component}>
        <div className={css.sidebar}>
            <Sidebar />
        </div>
        <div className={css.header}>
            <Header />
        </div>
        <div className={css.main}>{children}</div>
    </div>
);

export { MainLayout };
