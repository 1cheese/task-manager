import * as React from 'react';

import * as css from './PageTitle.scss';

type PageTitleProps = {
    label: string | React.ReactNode;
    buttons: React.ReactNodeArray;
};

const PageTitle: React.FC<PageTitleProps> = ({ label, buttons }) => (
    <div className={css.component}>
        <div className={css.label}>{label}</div>
        <div className={css.buttons}>
            {buttons.map((button, index) => (
                <React.Fragment key={index}>{button}</React.Fragment>
            ))}
        </div>
    </div>
);

export { PageTitle };
