import * as React from 'react';
import { Link } from 'react-router-dom';

import * as css from './Sidebar.scss';
import { HostawayLogo } from '../../IconsModule/components/HostawayLogo';
import { IndexRoute } from '../routes/IndexRoute';

function renderGroupOfItems(numberOfItems: number): React.ReactNode {
    return (
        <div className={css.group}>
            {new Array(numberOfItems).fill('').map((element, index) => (
                <div className={css.item} key={index} />
            ))}
        </div>
    );
}

const Sidebar: React.FC = () => (
    <div className={css.component}>
        <div className={css.logo}>
            <Link to={IndexRoute.url()}>
                <HostawayLogo />
            </Link>
        </div>
        {renderGroupOfItems(6)}
        {renderGroupOfItems(3)}
        {renderGroupOfItems(1)}
    </div>
);

export { Sidebar };
