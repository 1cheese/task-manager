import * as React from 'react';
import { Redirect, Route, RouteProps } from 'react-router-dom';

import { IndexRoute } from '../routes/IndexRoute';
import { TasksListRoute } from '../../../TasksPackage/TasksListModule/routes/TasksListRoute';

type Props = RouteProps & {};

export class IndexRouter extends React.Component<Props> {
    public static readonly defaultProps: Partial<Props> = {
        exact: true,
        path: IndexRoute.url(),
    };

    public render() {
        return (
            <Route
                {...this.props}
                component={() => <Redirect to={TasksListRoute.url()} />}
            />
        );
    }
}
