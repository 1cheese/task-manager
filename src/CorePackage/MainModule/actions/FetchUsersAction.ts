import { Action } from 'redux';
import { AsyncAction } from 'redux-promise-middleware';

import { UsersEndpoint } from '../../ApiModule/endpoints/UsersEndpoint';
import { createUser } from '../utils/createUser';
import { IUser } from '../interfaces/IUser';

export class FetchUsersAction implements AsyncAction {
    public static readonly type = 'FETCH_USERS';

    public readonly type = FetchUsersAction.type;
    public readonly payload = UsersEndpoint.getUsers().then(usersDTO =>
        usersDTO.map(createUser),
    );

    public constructor() {}
}

export class FetchUsersPendingAction implements Action {
    public static readonly type = 'FETCH_USERS_PENDING';

    public readonly type = FetchUsersPendingAction.type;
}

export class FetchUsersRejectedAction implements Action {
    public static readonly type = 'FETCH_USERS_REJECTED';

    public readonly type = FetchUsersRejectedAction.type;
}

export class FetchUsersFulfilledAction implements Action {
    public static readonly type = 'FETCH_USERS_FULFILLED';

    public readonly type = FetchUsersFulfilledAction.type;
    public readonly payload: IUser[] = [];
}
