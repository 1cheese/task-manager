import {
    applyMiddleware,
    combineReducers,
    compose,
    createStore,
    Reducer,
} from 'redux';
import thunk from 'redux-thunk';
import promiseMiddleware from 'redux-promise-middleware';

import { classActionMiddleware } from '../middlewares/classActionMiddleware';
import { IRootState } from '../interfaces/IRootState';
import { appStateReducer } from '../reducers';
import { tasksListReducer } from '../../../TasksPackage/TasksListModule/reducers';
import { createAndEditTaskReducer } from '../../../TasksPackage/CreateAndEditModule/reducers';

export function configureStore() {
    const reducer: Reducer<IRootState> = combineReducers({
        App: appStateReducer,
        TasksList: tasksListReducer,
        CreateAndEditTask: createAndEditTaskReducer,
    });
    const extendedCompose = (window as any)
        .__REDUX_DEVTOOLS_EXTENSION_COMPOSE__;
    const composeEnhancers =
        typeof extendedCompose === 'undefined' ? compose : extendedCompose;

    return createStore(
        reducer,
        composeEnhancers(
            applyMiddleware(classActionMiddleware, thunk, promiseMiddleware),
        ),
    );
}
