import { IAppState } from '../interfaces/IAppState';

export const defaultState: IAppState = {
    isFetching: false,
    users: [],
};
