import { IUserDTO } from '../interfaces/IUserDTO';
import { IUser } from '../interfaces/IUser';

export function createUser(userDTO: IUserDTO): IUser {
    return {
        id: userDTO.id,
        firstName: userDTO.first_name,
        lastName: userDTO.last_name,
    };
}
