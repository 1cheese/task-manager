import { createSelector } from 'reselect';

import { getAppState } from './getAppState';

export const getUsersList = createSelector(getAppState, state => state.users);
