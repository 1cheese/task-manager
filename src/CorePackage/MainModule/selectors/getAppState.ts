import { createSelector } from 'reselect';

import { IRootState } from '../interfaces/IRootState';

export const getAppState = createSelector(
    (state: IRootState) => state,
    state => state.App,
);
