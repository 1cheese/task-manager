import { createSelector } from 'reselect';

import { getAppState } from './getAppState';

export const getAppIsFetching = createSelector(
    getAppState,
    state => state.isFetching,
);
