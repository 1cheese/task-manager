import { createSelector } from 'reselect';

import { getUsersList } from './getUsersList';
import { IUser } from '../interfaces/IUser';

export const getUserById = (userId: IUser['id']) =>
    createSelector(getUsersList, users => {
        const user = users.find(user => user.id === userId);

        if (user === undefined) {
            return undefined;
        }

        return `${user.firstName} ${user.lastName}`;
    });
