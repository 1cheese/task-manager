import { HttpMethod } from '../enums/HttpMethod';
import { MimeType } from '../enums/MimeType';
import { HttpCredentials } from '../enums/HttpCredentials';
import { ErrorHandler } from '../common/ErrorHandler';
import { ResponseHandler } from '../common/ResponseHandler';
import { endpoint } from '../common/endpoint';
import { IUserDTO } from '../../MainModule/interfaces/IUserDTO';

export class UsersEndpoint {
    public static getUsers(): Promise<IUserDTO[]> {
        return fetch(endpoint`/api/users`, {
            method: HttpMethod.GET,
            credentials: HttpCredentials.SameOrigin,
            headers: {
                Accept: MimeType.APPLICATION_JSON,
            },
        })
            .then(ErrorHandler.commonHandler)
            .then(ResponseHandler.commonHandler)
            .catch(error => console.error(error));
    }
}
