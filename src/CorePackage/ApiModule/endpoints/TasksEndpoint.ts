import qs from 'qs';

import { HttpMethod } from '../enums/HttpMethod';
import { MimeType } from '../enums/MimeType';
import { HttpCredentials } from '../enums/HttpCredentials';
import { ErrorHandler } from '../common/ErrorHandler';
import { ResponseHandler } from '../common/ResponseHandler';
import { endpoint } from '../common/endpoint';
import { ITask } from '../../../TasksPackage/MainModule/interfaces/ITask';

export class TasksEndpoint {
    public static getTasks(): Promise<ITask[]> {
        return fetch(endpoint`/api/tasks`, {
            method: HttpMethod.GET,
            credentials: HttpCredentials.SameOrigin,
            headers: {
                Accept: MimeType.APPLICATION_JSON,
            },
        })
            .then(ErrorHandler.commonHandler)
            .then(ResponseHandler.commonHandler)
            .catch(error => console.error(error));
    }

    public static createTask(formData: ITask): Promise<ITask> {
        const serializedFormData = qs.stringify(formData, {
            encode: false,
        });
        const requestBody = new URLSearchParams(serializedFormData);

        return fetch(endpoint`/api/tasks`, {
            method: HttpMethod.POST,
            body: requestBody,
            credentials: HttpCredentials.SameOrigin,
            headers: {
                Accept: MimeType.APPLICATION_JSON,
            },
        })
            .then(ErrorHandler.commonHandler)
            .then(ResponseHandler.commonHandler)
            .catch(error => console.error(error));
    }

    public static editTask(formData: ITask): Promise<ITask> {
        const serializedFormData = qs.stringify(formData, {
            encode: false,
        });
        const requestBody = new URLSearchParams(serializedFormData);

        return fetch(endpoint`/api/tasks/${formData.id}`, {
            method: HttpMethod.PUT,
            body: requestBody,
            credentials: HttpCredentials.SameOrigin,
            headers: {
                Accept: MimeType.APPLICATION_JSON,
            },
        })
            .then(ErrorHandler.commonHandler)
            .then(ResponseHandler.commonHandler)
            .catch(error => console.error(error));
    }

    public static getTaskById(taskId: ITask['id']): Promise<ITask> {
        return fetch(endpoint`/api/tasks/${taskId}`, {
            method: HttpMethod.GET,
            credentials: HttpCredentials.SameOrigin,
            headers: {
                Accept: MimeType.APPLICATION_JSON,
            },
        })
            .then(ErrorHandler.commonHandler)
            .then(ResponseHandler.commonHandler)
            .catch(error => console.error(error));
    }

    public static deleteTaskById(taskId: ITask['id']): Promise<void> {
        return fetch(endpoint`/api/tasks/${taskId}`, {
            method: HttpMethod.DELETE,
            credentials: HttpCredentials.SameOrigin,
            headers: {
                Accept: MimeType.APPLICATION_JSON,
            },
        })
            .then(ErrorHandler.commonHandler)
            .then(ResponseHandler.commonHandler)
            .catch(error => console.error(error));
    }
}
