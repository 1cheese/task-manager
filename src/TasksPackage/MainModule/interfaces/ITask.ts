import { IUser } from '../../../CorePackage/MainModule/interfaces/IUser';

export interface ITask {
    id: string;
    name: string;
    assigneeId: IUser['id'];
    startsAt: string;
    dueAt: string;
    description: string;
}
