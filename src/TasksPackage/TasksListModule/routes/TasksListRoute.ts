import { AbstractRoute, route } from '../../../CorePackage/RouteModule';

@route()
export class TasksListRoute extends AbstractRoute {
    public static url() {
        return `/tasks`;
    }
}
