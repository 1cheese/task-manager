import { createSelector } from 'reselect';
import { getTasksState } from './getTasksState';

export const getTasksFilters = createSelector(
    getTasksState,
    tasksState => tasksState.filters,
);
