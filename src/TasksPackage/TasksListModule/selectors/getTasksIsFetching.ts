import { createSelector } from 'reselect';
import { getTasksState } from './getTasksState';

export const getTasksIsFetching = createSelector(
    getTasksState,
    tasksState => tasksState.isFetching,
);
