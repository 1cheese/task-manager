import { createSelector } from 'reselect';

import { IRootState } from '../../../CorePackage/MainModule/interfaces/IRootState';

export const getTasksState = createSelector(
    (state: IRootState) => state,
    state => state.TasksList,
);
