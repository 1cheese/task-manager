import { createSelector } from 'reselect';
import { isSameDay } from 'date-fns';

import { getTasksList } from './getTasksList';
import { getTasksFilters } from './getTasksFilters';

export const getFilteredTasks = createSelector(
    getTasksList,
    getTasksFilters,
    (tasks, tasksFilters) => {
        let tasksToReturn = [...tasks];

        tasksFilters.forEach(taskFilter => {
            switch (taskFilter.key) {
                case 'assigneeId': {
                    tasksToReturn = [...tasksToReturn].filter(
                        currentTask =>
                            currentTask.assigneeId === taskFilter.value,
                    );

                    break;
                }

                case 'startsAt': {
                    tasksToReturn = [...tasksToReturn].filter(currentTask =>
                        isSameDay(
                            new Date(Number(currentTask.startsAt)),
                            new Date(taskFilter.value as string),
                        ),
                    );

                    break;
                }

                case 'dueAt': {
                    tasksToReturn = [...tasksToReturn].filter(currentTask =>
                        isSameDay(
                            new Date(Number(currentTask.dueAt)),
                            new Date(taskFilter.value as string),
                        ),
                    );

                    break;
                }
            }
        });

        return tasksToReturn;
    },
);
