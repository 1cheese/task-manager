import { createSelector } from 'reselect';
import { getTasksState } from './getTasksState';

export const getTasksList = createSelector(
    getTasksState,
    tasksState => tasksState.tasks,
);
