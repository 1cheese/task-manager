import * as React from 'react';
import { Route, RouteProps } from 'react-router';

import { TasksListRoute } from '../routes/TasksListRoute';
import { TasksListPage } from '../components/TasksListPage';

type Props = RouteProps & {};

export class TasksListRouter extends React.Component<Props> {
    public static readonly defaultProps: Partial<Props> = {
        exact: true,
        path: TasksListRoute.url(),
    };

    public render() {
        return <Route {...this.props} component={TasksListPage} />;
    }
}
