import { ThunkAction } from '../../../../types/redux-thunk-promise';

import { IRootState } from '../../../CorePackage/MainModule/interfaces/IRootState';
import { ITask } from '../../MainModule/interfaces/ITask';
import {
    DeleteTaskAction,
    DeleteTaskFulfilledAction,
} from '../actions/DeleteTaskAction';
import { FetchTasksAction } from '../actions/FetchTasksAction';

export function deleteTask(taskId: ITask['id']): ThunkAction<void, IRootState> {
    return async dispatch => {
        const deleteAction = await dispatch(new DeleteTaskAction(taskId));

        if (deleteAction.action.type === DeleteTaskFulfilledAction.type) {
            await dispatch(new FetchTasksAction());
        }
    };
}
