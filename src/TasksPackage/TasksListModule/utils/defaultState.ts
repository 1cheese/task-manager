import { ITasksListState } from '../interfaces/ITasksListState';

export const defaultState: ITasksListState = {
    isFetching: false,
    tasks: [],
    filters: [],
};
