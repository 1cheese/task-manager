import * as React from 'react';
import { useDispatch } from 'react-redux';

import * as css from './DeleteTaskButton.scss';
import { ITask } from '../../MainModule/interfaces/ITask';
import { TrashBinIcon } from '../../../CorePackage/IconsModule/components/TrashBinIcon';
import { Modal } from '../../../CorePackage/PopupModule/components/Modal';
import { Button } from '../../../CorePackage/InputsModule/components/Button';
import { ButtonStyle } from '../../../CorePackage/InputsModule/enums/ButtonStyle';
import { PopupPortal } from '../../../CorePackage/PopupModule/components/PopupPortal';
import { deleteTask } from '../thunks/deleteTask';

type DeleteTaskButtonProps = {
    taskId: ITask['id'];
};

const DeleteTaskButton: React.FC<DeleteTaskButtonProps> = ({ taskId }) => {
    const dispatch = useDispatch();
    const [isModalOpened, setIsModalOpened] = React.useState(false);

    const handleModalOpen = () => {
        setIsModalOpened(true);
    };

    const handleModalClose = () => {
        setIsModalOpened(false);
    };

    const handleCancelConfirmButtonClick = () => {
        dispatch(deleteTask(taskId));
    };

    return (
        <>
            <div className={css.component} onClick={handleModalOpen}>
                <TrashBinIcon />
            </div>

            <PopupPortal>
                <Modal
                    isOpen={isModalOpened}
                    title={'Are you sure you want to delete?'}
                    onClose={handleModalClose}
                    buttons={[
                        <Button onClick={handleModalClose}>Cancel</Button>,
                        <Button
                            buttonStyle={ButtonStyle.BLUE}
                            onClick={handleCancelConfirmButtonClick}
                        >
                            OK
                        </Button>,
                    ]}
                />
            </PopupPortal>
        </>
    );
};

export { DeleteTaskButton };
