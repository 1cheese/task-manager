import * as React from 'react';
import { useDispatch } from 'react-redux';
import { nanoid } from 'nanoid';

import {
    Modal,
    ModalProps,
} from '../../../CorePackage/PopupModule/components/Modal';
import { Button } from '../../../CorePackage/InputsModule/components/Button';
import { ButtonStyle } from '../../../CorePackage/InputsModule/enums/ButtonStyle';
import { Input } from '../../../CorePackage/InputsModule/components/Input';
import { InputField } from '../../../CorePackage/InputsModule/components/InputField';
import { SetTaskFieldByAliasAction } from '../../CreateAndEditModule/actions/SetTaskFieldByAliasAction';
import { TaskFieldAlias } from '../../CreateAndEditModule/enums/TaskFieldAlias';
import { browserHistory } from '../../../CorePackage/MainModule/utils/browserHistory';
import { CreateTaskRoute } from '../../CreateAndEditModule/routes/CreateTaskRoute';

type CreateTaskModalProps = Pick<ModalProps, 'isOpen' | 'onClose'>;

const CreateTaskModal: React.FC<CreateTaskModalProps> = ({
    isOpen,
    onClose,
}) => {
    const id = `id_${nanoid(4)}`;
    const [inputValue, setInputValue] = React.useState('');
    const dispatch = useDispatch();

    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { value } = event.target;
        setInputValue(value);
    };

    const handleModalClose = () => {
        setInputValue('');
        onClose();
    };

    const handleSubmit = () => {
        dispatch(
            new SetTaskFieldByAliasAction({
                fieldAlias: TaskFieldAlias.NAME,
                value: inputValue,
            }),
        );
        setInputValue('');
        onClose();
        browserHistory.push(CreateTaskRoute.url());
    };

    const handleInputKeyDown = (
        event: React.KeyboardEvent<HTMLInputElement>,
    ): void => {
        if (event.key === 'Enter') {
            handleSubmit();
        }
    };

    return (
        <Modal
            isOpen={isOpen}
            title={'Create a new task'}
            onClose={handleModalClose}
            buttons={[
                <Button
                    buttonStyle={ButtonStyle.BLUE_WITHOUT_BG}
                    onClick={handleSubmit}
                >
                    Create task
                </Button>,
            ]}
        >
            <InputField label={'Task name'} inputId={id}>
                <Input
                    id={id}
                    value={inputValue}
                    onChange={handleInputChange}
                    onKeyDown={handleInputKeyDown}
                />
            </InputField>
        </Modal>
    );
};

export { CreateTaskModal };
