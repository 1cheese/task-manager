import * as React from 'react';
import { nanoid } from 'nanoid';

import { IUser } from '../../../CorePackage/MainModule/interfaces/IUser';
import { InputField } from '../../../CorePackage/InputsModule/components/InputField';
import { Select } from '../../../CorePackage/InputsModule/components/Select';

type TasksListAssigneeFilterProps = {
    users: IUser[];
    onChange: (value: IUser['id']) => void;
};

const TasksListAssigneeFilter: React.FC<TasksListAssigneeFilterProps> = ({
    users,
    onChange,
}) => {
    const id = `id_${nanoid(4)}`;
    const [value, setValue] = React.useState('');

    const handleSelectChange = (
        event: React.ChangeEvent<HTMLSelectElement>,
    ) => {
        const { value } = event.target;
        setValue(value);
        onChange(value);
    };

    return (
        <InputField label="Assignee" inputId={id}>
            <Select
                placeholder="No assignee selected"
                id={id}
                value={value}
                onChange={handleSelectChange}
                options={users.map(({ id, firstName, lastName }) => ({
                    value: id,
                    label: `${firstName} ${lastName}`,
                    disabled: false,
                }))}
            />
        </InputField>
    );
};

export { TasksListAssigneeFilter, TasksListAssigneeFilterProps };
