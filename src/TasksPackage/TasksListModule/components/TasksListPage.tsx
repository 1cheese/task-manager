import * as React from 'react';
import Container from 'react-bootstrap/cjs/Container';
import Row from 'react-bootstrap/cjs/Row';
import Col from 'react-bootstrap/cjs/Col';

import * as css from './TasksListPage.scss';
import { PageTitle } from '../../../CorePackage/MainModule/components/PageTitle';
import { Button } from '../../../CorePackage/InputsModule/components/Button';
import { ButtonStyle } from '../../../CorePackage/InputsModule/enums/ButtonStyle';
import { PlusIcon } from '../../../CorePackage/IconsModule/components/PlusIcon';
import { TasksListTableContainer } from '../containers/TasksListTableContainer';
import { TasksListAssigneeFilterContainer } from '../containers/TasksListAssigneeFilterContainer';
import { TasksListDateFilterContainer } from '../containers/TasksListDateFilterContainer';
import { PopupPortal } from '../../../CorePackage/PopupModule/components/PopupPortal';
import { CreateTaskModal } from './CreateTaskModal';

const TasksListPage: React.FC = () => {
    const [isModalOpened, setIsModalOpened] = React.useState(false);

    const handleModalOpen = () => {
        setIsModalOpened(true);
    };

    const handleModalClose = () => {
        setIsModalOpened(false);
    };

    return (
        <>
            <PageTitle
                label="Tasks"
                buttons={[
                    <Button
                        buttonStyle={ButtonStyle.BLUE}
                        className={css.button}
                        onClick={handleModalOpen}
                    >
                        <PlusIcon />
                        <span>Add task</span>
                    </Button>,
                ]}
            />
            <div className={css.filters}>
                <Container fluid>
                    <Row>
                        <Col
                            xs={{
                                span: 3,
                                offset: 1,
                            }}
                        >
                            <TasksListAssigneeFilterContainer />
                        </Col>
                        <Col xs={2}>
                            <TasksListDateFilterContainer
                                label={'Starting date'}
                                fieldKey={'startsAt'}
                            />
                        </Col>
                        <Col xs={2}>
                            <TasksListDateFilterContainer
                                label={'Due date'}
                                fieldKey={'dueAt'}
                            />
                        </Col>
                    </Row>
                </Container>
            </div>
            <div className={css.table}>
                <TasksListTableContainer />
            </div>

            <PopupPortal>
                <CreateTaskModal
                    isOpen={isModalOpened}
                    onClose={handleModalClose}
                />
            </PopupPortal>
        </>
    );
};

export { TasksListPage };
