import * as React from 'react';
import { format } from 'date-fns';

type TasksTableDateCellProps = {
    timestamp: string;
};

const TasksTableDateCell: React.FC<TasksTableDateCellProps> = ({
    timestamp,
}) => <>{format(new Date(Number(timestamp)), 'dd MMMM yyyy - HH:mm')}</>;

export { TasksTableDateCell };
