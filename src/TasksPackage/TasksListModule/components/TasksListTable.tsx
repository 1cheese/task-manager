import * as React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';

import * as css from './TasksListTable.scss';
import { ITask } from '../../MainModule/interfaces/ITask';
import { PencilIcon } from '../../../CorePackage/IconsModule/components/PencilIcon';
import { TasksTableDateCell } from './TasksTableDateCell';
import { TasksTableAssigneeCellContainer } from '../containers/TasksTableAssigneeCellContainer';
import { Preloader } from '../../../CorePackage/IconsModule/components/Preloader';
import { DeleteTaskButton } from './DeleteTaskButton';
import { Link } from 'react-router-dom';
import { EditTaskRoute } from '../../CreateAndEditModule/routes/EditTaskRoute';

type TasksListProps = {
    tasks: ITask[];
    isFetching: boolean;
    onMount: () => void;
};

const TasksListTable: React.FC<TasksListProps> = ({
    isFetching,
    tasks,
    onMount,
}) => {
    React.useEffect(() => {
        onMount();
    }, []);

    if (isFetching === true) {
        return (
            <div className={css.preloader}>
                <Preloader />
            </div>
        );
    }

    return (
        <Container fluid>
            <Row>
                <Col
                    xs={{
                        span: 10,
                        offset: 1,
                    }}
                >
                    <div className={css.table}>
                        <div className={css.tableHead}>
                            <Container fluid>
                                <Row>
                                    <Col xs={3}>
                                        <div className={css.tableHeadCell}>
                                            Task name
                                        </div>
                                    </Col>
                                    <Col xs={2}>
                                        <div className={css.tableHeadCell}>
                                            Assignee
                                        </div>
                                    </Col>
                                    <Col xs={3}>
                                        <div className={css.tableHeadCell}>
                                            Starts at
                                        </div>
                                    </Col>
                                    <Col xs={3}>
                                        <div className={css.tableHeadCell}>
                                            Due at
                                        </div>
                                    </Col>
                                </Row>
                            </Container>
                        </div>
                        {tasks.length === 0 ? (
                            <div className={css.tableBodyRow}>
                                <span className={css.warning}>No matches</span>
                            </div>
                        ) : (
                            tasks.map(task => (
                                <div className={css.tableBodyRow} key={task.id}>
                                    <Container fluid>
                                        <Row>
                                            <Col xs={3}>
                                                <div
                                                    className={
                                                        css.tableBodyCell
                                                    }
                                                >
                                                    {task.name}
                                                </div>
                                            </Col>
                                            <Col xs={2}>
                                                <div
                                                    className={
                                                        css.tableBodyCell
                                                    }
                                                >
                                                    <TasksTableAssigneeCellContainer
                                                        assigneeId={
                                                            task.assigneeId
                                                        }
                                                    />
                                                </div>
                                            </Col>
                                            <Col xs={3}>
                                                <div
                                                    className={
                                                        css.tableBodyCell
                                                    }
                                                >
                                                    <TasksTableDateCell
                                                        timestamp={
                                                            task.startsAt
                                                        }
                                                    />
                                                </div>
                                            </Col>
                                            <Col xs={3}>
                                                <div
                                                    className={
                                                        css.tableBodyCell
                                                    }
                                                >
                                                    <TasksTableDateCell
                                                        timestamp={task.dueAt}
                                                    />
                                                </div>
                                            </Col>
                                            <Col xs={1}>
                                                <div
                                                    className={
                                                        css.tableBodyCell
                                                    }
                                                >
                                                    <div className={css.icons}>
                                                        <Link
                                                            to={EditTaskRoute.url(
                                                                {
                                                                    id: task.id,
                                                                },
                                                            )}
                                                            className={
                                                                css.editLink
                                                            }
                                                        >
                                                            <PencilIcon />
                                                        </Link>
                                                        <DeleteTaskButton
                                                            taskId={task.id}
                                                        />
                                                    </div>
                                                </div>
                                            </Col>
                                        </Row>
                                    </Container>
                                </div>
                            ))
                        )}
                    </div>
                </Col>
            </Row>
        </Container>
    );
};

export { TasksListTable, TasksListProps };
