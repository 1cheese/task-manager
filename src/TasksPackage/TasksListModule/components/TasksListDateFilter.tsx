import * as React from 'react';

import { InputField } from '../../../CorePackage/InputsModule/components/InputField';
import { DatepickerInput } from '../../../CorePackage/InputsModule/components/DatepickerInput';

type TasksListDateFilterProps = {
    label: string;
    onChange: (date: Date | undefined) => void;
};

const TasksListDateFilter: React.FC<TasksListDateFilterProps> = ({
    label,
    onChange,
}) => {
    const [value, setValue] = React.useState<Date | undefined>(undefined);

    const handleInputChange = (date: Date | undefined): void => {
        setValue(date);
        onChange(date);
    };

    return (
        <InputField label={label}>
            <DatepickerInput onDayChange={handleInputChange} value={value} />
        </InputField>
    );
};

export { TasksListDateFilter, TasksListDateFilterProps };
