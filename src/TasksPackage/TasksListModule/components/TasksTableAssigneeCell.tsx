import * as React from 'react';

type TasksTableAssigneeCellProps = {
    assigneeFullName?: string;
};

const TasksTableAssigneeCell: React.FC<TasksTableAssigneeCellProps> = ({
    assigneeFullName,
}) => {
    if (assigneeFullName === undefined) {
        return null;
    }

    return <>{assigneeFullName}</>;
};

export { TasksTableAssigneeCell, TasksTableAssigneeCellProps };
