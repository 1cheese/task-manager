import { connect, MapDispatchToProps, MapStateToProps } from 'react-redux';

import { TasksListTable, TasksListProps } from '../components/TasksListTable';
import { FetchTasksAction } from '../actions/FetchTasksAction';
import { getTasksIsFetching } from '../selectors/getTasksIsFetching';
import { getFilteredTasks } from '../selectors/getFilteredTasks';
import { IRootState } from '../../../CorePackage/MainModule/interfaces/IRootState';
import { FetchUsersAction } from '../../../CorePackage/MainModule/actions/FetchUsersAction';

type OwnProps = Pick<TasksListProps, never>;
type StateProps = Pick<TasksListProps, 'isFetching' | 'tasks'>;
type DispatchProps = Pick<TasksListProps, 'onMount'>;

const mapStateToProps: MapStateToProps<
    StateProps,
    OwnProps,
    IRootState
> = state => ({
    isFetching: getTasksIsFetching(state),
    tasks: getFilteredTasks(state),
});

const mapDispatchToProps: MapDispatchToProps<
    DispatchProps,
    OwnProps
> = dispatch => ({
    onMount: () => {
        dispatch(new FetchUsersAction());
        dispatch(new FetchTasksAction());
    },
});

export const TasksListTableContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(TasksListTable);
