import { connect, MapDispatchToProps, MapStateToProps } from 'react-redux';

import {
    TasksListAssigneeFilter,
    TasksListAssigneeFilterProps,
} from '../components/TasksListAssigneeFilter';
import { IRootState } from '../../../CorePackage/MainModule/interfaces/IRootState';
import { getUsersList } from '../../../CorePackage/MainModule/selectors/getUsersList';
import { FilterTasksAction } from '../actions/FilterTasksAction';

type OwnProps = Pick<TasksListAssigneeFilterProps, never>;
type StateProps = Pick<TasksListAssigneeFilterProps, 'users'>;
type DispatchProps = Pick<TasksListAssigneeFilterProps, 'onChange'>;

const mapStateToProps: MapStateToProps<
    StateProps,
    OwnProps,
    IRootState
> = state => ({
    users: getUsersList(state),
});

const mapDispatchToProps: MapDispatchToProps<
    DispatchProps,
    OwnProps
> = dispatch => ({
    onChange: value => {
        dispatch(
            new FilterTasksAction({
                key: 'assigneeId',
                value: value.length === 0 ? null : value,
            }),
        );
    },
});

export const TasksListAssigneeFilterContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(TasksListAssigneeFilter);
