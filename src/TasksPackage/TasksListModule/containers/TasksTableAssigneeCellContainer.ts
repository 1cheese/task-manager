import { connect, MapStateToProps } from 'react-redux';

import {
    TasksTableAssigneeCell,
    TasksTableAssigneeCellProps,
} from '../components/TasksTableAssigneeCell';
import { IRootState } from '../../../CorePackage/MainModule/interfaces/IRootState';
import { getUserById } from '../../../CorePackage/MainModule/selectors/getUserById';
import { IUser } from '../../../CorePackage/MainModule/interfaces/IUser';

type OwnProps = { assigneeId: IUser['id'] };
type StateProps = Pick<TasksTableAssigneeCellProps, 'assigneeFullName'>;

const mapStateToProps: MapStateToProps<StateProps, OwnProps, IRootState> = (
    state,
    ownProps,
) => ({
    assigneeFullName: getUserById(ownProps.assigneeId)(state),
});

export const TasksTableAssigneeCellContainer = connect(mapStateToProps)(
    TasksTableAssigneeCell,
);
