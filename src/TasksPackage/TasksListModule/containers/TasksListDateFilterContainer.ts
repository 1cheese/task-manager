import { connect, MapDispatchToProps } from 'react-redux';
import { format } from 'date-fns';

import { FilterTasksAction } from '../actions/FilterTasksAction';
import {
    TasksListDateFilter,
    TasksListDateFilterProps,
} from '../components/TasksListDateFilter';
import { ITask } from '../../MainModule/interfaces/ITask';
import { DATE_FORMAT } from '../../../CorePackage/MainModule/constants/DATE_FORMAT';

type OwnProps = { fieldKey: keyof Pick<ITask, 'startsAt' | 'dueAt'> };
type DispatchProps = Pick<TasksListDateFilterProps, 'onChange'>;

const mapDispatchToProps: MapDispatchToProps<DispatchProps, OwnProps> = (
    dispatch,
    ownProps,
) => ({
    onChange: date => {
        dispatch(
            new FilterTasksAction({
                key: ownProps.fieldKey,
                value: date === undefined ? null : format(date, DATE_FORMAT),
            }),
        );
    },
});

export const TasksListDateFilterContainer = connect(
    null,
    mapDispatchToProps,
)(TasksListDateFilter);
