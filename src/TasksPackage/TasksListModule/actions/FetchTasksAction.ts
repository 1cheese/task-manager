import { Action } from 'redux';
import { AsyncAction } from 'redux-promise-middleware';

import { TasksEndpoint } from '../../../CorePackage/ApiModule/endpoints/TasksEndpoint';
import { ITask } from '../../MainModule/interfaces/ITask';

export class FetchTasksAction implements AsyncAction {
    public static readonly type = 'FETCH_TASKS';

    public readonly type = FetchTasksAction.type;
    public readonly payload = TasksEndpoint.getTasks();

    public constructor() {}
}

export class FetchTasksPendingAction implements Action {
    public static readonly type = 'FETCH_TASKS_PENDING';

    public readonly type = FetchTasksPendingAction.type;
}

export class FetchTasksRejectedAction implements Action {
    public static readonly type = 'FETCH_TASKS_REJECTED';

    public readonly type = FetchTasksRejectedAction.type;
}

export class FetchTasksFulfilledAction implements Action {
    public static readonly type = 'FETCH_TASKS_FULFILLED';

    public readonly type = FetchTasksFulfilledAction.type;
    public readonly payload: ITask[] = [];
}
