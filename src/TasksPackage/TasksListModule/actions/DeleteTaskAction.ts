import { Action } from 'redux';
import { AsyncAction } from 'redux-promise-middleware';

import { TasksEndpoint } from '../../../CorePackage/ApiModule/endpoints/TasksEndpoint';
import { ITask } from '../../MainModule/interfaces/ITask';

export class DeleteTaskAction implements AsyncAction {
    public static readonly type = 'DELETE_TASK';

    public readonly type = DeleteTaskAction.type;
    public readonly payload = TasksEndpoint.deleteTaskById(this.taskId);

    public constructor(private readonly taskId: ITask['id']) {}
}

export class DeleteTaskPendingAction implements Action {
    public static readonly type = 'DELETE_TASK_PENDING';

    public readonly type = DeleteTaskPendingAction.type;
}

export class DeleteTaskRejectedAction implements Action {
    public static readonly type = 'DELETE_TASK_REJECTED';

    public readonly type = DeleteTaskRejectedAction.type;
}

export class DeleteTaskFulfilledAction implements Action {
    public static readonly type = 'DELETE_TASK_FULFILLED';

    public readonly type = DeleteTaskFulfilledAction.type;
}
