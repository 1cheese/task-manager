import { Action } from 'redux';

import { ITasksListFilter } from '../interfaces/ITasksListFilter';

export class FilterTasksAction implements Action {
    public static readonly type = 'ADD_TASKS_LIST_FILTER';

    public readonly type = FilterTasksAction.type;

    public constructor(public readonly payload: ITasksListFilter) {}
}
