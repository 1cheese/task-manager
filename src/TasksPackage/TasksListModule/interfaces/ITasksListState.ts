import { ITask } from '../../MainModule/interfaces/ITask';
import { ITasksListFilter } from './ITasksListFilter';

export interface ITasksListState {
    isFetching: boolean;
    tasks: ITask[];
    filters: ITasksListFilter[];
}
