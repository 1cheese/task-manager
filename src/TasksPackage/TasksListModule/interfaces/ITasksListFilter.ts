import { ITask } from '../../MainModule/interfaces/ITask';

export interface ITasksListFilter {
    key: keyof Omit<ITask, 'id' | 'description'>;
    value: string | null;
}
