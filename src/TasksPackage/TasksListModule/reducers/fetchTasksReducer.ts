import { Reducer } from 'redux';

import {
    FetchTasksAction,
    FetchTasksFulfilledAction,
    FetchTasksPendingAction,
    FetchTasksRejectedAction,
} from '../actions/FetchTasksAction';
import { ITasksListState } from '../interfaces/ITasksListState';
import { defaultState } from '../utils/defaultState';

type TAction =
    | FetchTasksAction
    | FetchTasksPendingAction
    | FetchTasksRejectedAction
    | FetchTasksFulfilledAction;

export const fetchTasksReducer: Reducer<ITasksListState, TAction> = (
    state = defaultState,
    action,
): ITasksListState => {
    switch (action.type) {
        case FetchTasksPendingAction.type:
            return {
                ...state,
                isFetching: true,
            };

        case FetchTasksRejectedAction.type:
            return {
                ...state,
                isFetching: false,
            };

        case FetchTasksFulfilledAction.type:
            return {
                ...state,
                isFetching: false,
                tasks: action.payload,
            };

        default:
            return state;
    }
};
