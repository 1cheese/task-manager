import { Reducer } from 'redux';

import {
    DeleteTaskAction,
    DeleteTaskFulfilledAction,
    DeleteTaskPendingAction,
    DeleteTaskRejectedAction,
} from '../actions/DeleteTaskAction';
import { ITasksListState } from '../interfaces/ITasksListState';
import { defaultState } from '../utils/defaultState';

type TAction =
    | DeleteTaskAction
    | DeleteTaskPendingAction
    | DeleteTaskRejectedAction
    | DeleteTaskFulfilledAction;

export const deleteTaskReducer: Reducer<ITasksListState, TAction> = (
    state = defaultState,
    action,
): ITasksListState => {
    switch (action.type) {
        case DeleteTaskPendingAction.type:
            return {
                ...state,
                isFetching: true,
            };

        case DeleteTaskRejectedAction.type:
            return {
                ...state,
                isFetching: false,
            };

        case DeleteTaskFulfilledAction.type:
            return {
                ...state,
                isFetching: false,
            };

        default:
            return state;
    }
};
