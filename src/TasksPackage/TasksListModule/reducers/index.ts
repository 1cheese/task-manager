import reduceReducers from 'reduce-reducers';

import { defaultState } from '../utils/defaultState';
import { fetchTasksReducer } from './fetchTasksReducer';
import { ITasksListState } from '../interfaces/ITasksListState';
import { filterTasksReducer } from './filterTasksReducer';
import { deleteTaskReducer } from './deleteTaskReducer';

export const tasksListReducer = reduceReducers<ITasksListState, any>(
    defaultState ?? null,
    fetchTasksReducer,
    filterTasksReducer,
    deleteTaskReducer,
);
