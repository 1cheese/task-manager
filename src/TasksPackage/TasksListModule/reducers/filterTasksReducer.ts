import { Reducer } from 'redux';

import { FilterTasksAction } from '../actions/FilterTasksAction';
import { ITasksListState } from '../interfaces/ITasksListState';
import { defaultState } from '../utils/defaultState';

type TAction = FilterTasksAction;

export const filterTasksReducer: Reducer<ITasksListState, TAction> = (
    state = defaultState,
    action,
): ITasksListState => {
    switch (action.type) {
        case FilterTasksAction.type:
            const shouldRemoveFilter = action.payload.value === null;
            const hasFilterOfSpecificKey = [...state.filters].some(
                currentFilter => currentFilter.key === action.payload.key,
            );

            return {
                ...state,
                filters:
                    shouldRemoveFilter === true
                        ? state.filters.filter(
                              currentFilter =>
                                  currentFilter.key !== action.payload.key,
                          )
                        : hasFilterOfSpecificKey
                        ? [...state.filters].map(currentFilter => ({
                              ...currentFilter,
                              value:
                                  currentFilter.key === action.payload.key
                                      ? action.payload.value
                                      : currentFilter.value,
                          }))
                        : [...state.filters, action.payload],
            };

        default:
            return state;
    }
};
