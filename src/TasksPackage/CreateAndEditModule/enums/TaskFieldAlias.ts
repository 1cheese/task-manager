export enum TaskFieldAlias {
    NAME = 'name',
    STARTING_DATE = 'startsAt',
    STARTING_TIME = 'startingTime',
    DUE_DATE = 'dueAt',
    DUE_TIME = 'dueTime',
    DESCRIPTION = 'description',
    ASSIGNEE_ID = 'assigneeId',
}
