import { createSelector } from 'reselect';

import { TaskFieldAlias } from '../enums/TaskFieldAlias';
import { getCreateAndEditTaskFields } from './getCreateAndEditTaskFields';

export const getTaskFieldByFieldAlias = (fieldAlias: TaskFieldAlias) =>
    createSelector(getCreateAndEditTaskFields, fields => fields[fieldAlias]);
