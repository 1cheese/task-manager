import { createSelector } from 'reselect';
import { getCreateAndEditTaskState } from './getCreateAndEditTaskState';

export const getCreateAndEditTaskFields = createSelector(
    getCreateAndEditTaskState,
    state => state.fields,
);
