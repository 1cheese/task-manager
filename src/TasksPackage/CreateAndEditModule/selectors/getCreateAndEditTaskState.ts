import { createSelector } from 'reselect';

import { IRootState } from '../../../CorePackage/MainModule/interfaces/IRootState';

export const getCreateAndEditTaskState = createSelector(
    (state: IRootState) => state,
    state => state.CreateAndEditTask,
);
