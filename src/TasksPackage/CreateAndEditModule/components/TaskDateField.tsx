import * as React from 'react';
import { nanoid } from 'nanoid';
import { format } from 'date-fns';

import { ITaskFieldProps } from '../interfaces/ITaskFieldProps';
import { InputField } from '../../../CorePackage/InputsModule/components/InputField';
import { DatepickerInput } from '../../../CorePackage/InputsModule/components/DatepickerInput';
import { DATE_FORMAT } from '../../../CorePackage/MainModule/constants/DATE_FORMAT';

const TaskDateField: React.FC<ITaskFieldProps> = ({
    value,
    label,
    onFieldChange,
}) => {
    const id = `id_${nanoid(4)}`;
    const handleFieldChange = (date: Date | undefined) => {
        if (date === undefined) {
            return;
        }

        onFieldChange(format(date, DATE_FORMAT));
    };

    return (
        <InputField label={label} inputId={id}>
            <DatepickerInput onDayChange={handleFieldChange} value={value} />
        </InputField>
    );
};

export { TaskDateField };
