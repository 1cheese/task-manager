import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { nanoid } from 'nanoid';

import * as css from './TaskAssigneeField.scss';
import { ITaskFieldProps } from '../interfaces/ITaskFieldProps';
import {
    Select,
    SelectProps,
} from '../../../CorePackage/InputsModule/components/Select';
import { InputField } from '../../../CorePackage/InputsModule/components/InputField';
import { IRootState } from '../../../CorePackage/MainModule/interfaces/IRootState';
import { IUser } from '../../../CorePackage/MainModule/interfaces/IUser';
import { getUsersList } from '../../../CorePackage/MainModule/selectors/getUsersList';
import { FetchUsersAction } from '../../../CorePackage/MainModule/actions/FetchUsersAction';

const TaskAssigneeField: React.FC<ITaskFieldProps> = ({
    value,
    label,
    onFieldChange,
}) => {
    const id = `id_${nanoid(4)}`;
    const dispatch = useDispatch();
    React.useEffect(() => {
        dispatch(new FetchUsersAction());
    }, []);

    const handleFieldChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
        onFieldChange(event.target.value);
    };

    const users = useSelector<IRootState, IUser[]>(getUsersList);
    const options: SelectProps['options'] = users.map(user => ({
        value: user.id,
        label: `${user.firstName} ${user.lastName}`,
        disabled: false,
    }));

    if (users.length === 0) {
        return null;
    }

    return (
        <InputField label={label} inputId={id}>
            <Select
                id={id}
                value={value}
                options={options}
                placeholder={'Please, choose assignee'}
                className={css.component}
                onChange={handleFieldChange}
            />
        </InputField>
    );
};

export { TaskAssigneeField };
