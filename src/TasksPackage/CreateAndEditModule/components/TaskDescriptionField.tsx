import * as React from 'react';
import { nanoid } from 'nanoid';

import * as css from './TaskDescriptionField.scss';
import { ITaskFieldProps } from '../interfaces/ITaskFieldProps';
import { InputField } from '../../../CorePackage/InputsModule/components/InputField';
import { TextArea } from '../../../CorePackage/InputsModule/components/TextArea';

const TaskDescriptionField: React.FC<ITaskFieldProps> = ({
    value,
    label,
    onFieldChange,
}) => {
    const id = `id_${nanoid(4)}`;

    const handleFieldChange = (
        event: React.ChangeEvent<HTMLTextAreaElement>,
    ) => {
        onFieldChange(event.target.value);
    };

    return (
        <InputField label={label} inputId={id}>
            <TextArea
                id={id}
                value={value}
                className={css.component}
                onChange={handleFieldChange}
            />
        </InputField>
    );
};

export { TaskDescriptionField };
