import * as React from 'react';
import { useDispatch } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';
import Col from 'react-bootstrap/cjs/Col';
import Row from 'react-bootstrap/cjs/Row';
import Container from 'react-bootstrap/cjs/Container';

import * as css from './CreateAndEditTaskPage.scss';
import { renderTaskFieldWithProps } from '../utils/renderTaskFieldWithProps';
import { TaskFieldAlias } from '../enums/TaskFieldAlias';
import { TaskTitleField } from './TaskTitleField';
import { PageTitle } from '../../../CorePackage/MainModule/components/PageTitle';
import { Button } from '../../../CorePackage/InputsModule/components/Button';
import { ButtonStyle } from '../../../CorePackage/InputsModule/enums/ButtonStyle';
import { ClearCreateAndEditTaskStateAction } from '../actions/ClearCreateAndEditTaskStateAction';
import { TasksListRoute } from '../../TasksListModule/routes/TasksListRoute';
import { TaskAssigneeField } from './TaskAssigneeField';
import { TaskDateField } from './TaskDateField';
import { TaskDescriptionField } from './TaskDescriptionField';
import { Modal } from '../../../CorePackage/PopupModule/components/Modal';
import { PopupPortal } from '../../../CorePackage/PopupModule/components/PopupPortal';
import { createTask } from '../thunks/createTask';
import { FetchTaskByIdAction } from '../actions/FetchTaskByIdAction';
import { editTask } from '../thunks/editTask';
import { browserHistory } from '../../../CorePackage/MainModule/utils/browserHistory';

const CreateAndEditTaskPage: React.FC<RouteComponentProps<{
    id?: string;
}>> = props => {
    const dispatch = useDispatch();
    const [isModalOpened, setIsModalOpened] = React.useState(false);
    const taskId = props.match.params.id;
    const isEditPage = taskId !== undefined;

    React.useEffect(() => {
        if (isEditPage === true) {
            dispatch(new FetchTaskByIdAction(taskId as string));
        }
    }, [taskId]);

    const handleDiscardButtonClick = () => {
        dispatch(new ClearCreateAndEditTaskStateAction());
        handleModalClose();
    };

    const handleModalOpen = () => {
        setIsModalOpened(true);
    };

    const handleModalClose = () => {
        setIsModalOpened(false);
    };

    const handleSaveButtonClick = () => {
        if (isEditPage === true) {
            dispatch(editTask(taskId as string));
            return;
        }

        dispatch(createTask());
    };

    const handleBackToTasksClick = () => {
        dispatch(new ClearCreateAndEditTaskStateAction());
        browserHistory.push(TasksListRoute.url());
    };

    return (
        <>
            <div className={css.back} onClick={handleBackToTasksClick}>
                Back to Tasks
            </div>
            <PageTitle
                label={renderTaskFieldWithProps(
                    TaskTitleField,
                    TaskFieldAlias.NAME,
                )}
                buttons={[
                    <Button
                        buttonStyle={ButtonStyle.GREEN}
                        onClick={handleSaveButtonClick}
                    >
                        Save
                    </Button>,
                    <Button onClick={handleModalOpen}>Cancel</Button>,
                ]}
            />
            <div className={css.main}>
                <Container fluid>
                    <Row>
                        <Col
                            xs={{
                                span: 6,
                                offset: 1,
                            }}
                        >
                            <div className={css.leftBlock}>
                                <Container fluid>
                                    <Row>
                                        <Col xs={12}>
                                            <h3 className={css.subheading}>
                                                Schedule
                                            </h3>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs={6}>
                                            {renderTaskFieldWithProps(
                                                TaskDateField,
                                                TaskFieldAlias.STARTING_DATE,
                                            )}
                                        </Col>
                                        <Col xs={6}>
                                            {renderTaskFieldWithProps(
                                                TaskDateField,
                                                TaskFieldAlias.DUE_DATE,
                                            )}
                                        </Col>
                                    </Row>
                                    <Row className={css.details}>
                                        <Col xs={12}>
                                            <h3 className={css.subheading}>
                                                Details
                                            </h3>
                                            {renderTaskFieldWithProps(
                                                TaskDescriptionField,
                                                TaskFieldAlias.DESCRIPTION,
                                            )}
                                        </Col>
                                    </Row>
                                </Container>
                            </div>
                        </Col>
                        <Col xs={5}>
                            <div className={css.rightBlock}>
                                <h3 className={css.subheading}>Users</h3>
                                {renderTaskFieldWithProps(
                                    TaskAssigneeField,
                                    TaskFieldAlias.ASSIGNEE_ID,
                                )}
                            </div>
                        </Col>
                    </Row>
                </Container>
            </div>

            <PopupPortal>
                <Modal
                    isOpen={isModalOpened}
                    title={'Discard unsaved changes'}
                    onClose={handleModalClose}
                    buttons={[
                        <Button onClick={handleModalClose}>Cancel</Button>,
                        <Button
                            buttonStyle={ButtonStyle.BLUE}
                            onClick={handleDiscardButtonClick}
                        >
                            Discard
                        </Button>,
                    ]}
                />
            </PopupPortal>
        </>
    );
};

export { CreateAndEditTaskPage };
