import * as React from 'react';

import * as css from './TaskTitleField.scss';
import { Input } from '../../../CorePackage/InputsModule/components/Input';
import { ITaskFieldProps } from '../interfaces/ITaskFieldProps';

const TaskTitleField: React.FC<ITaskFieldProps> = ({
    value,
    onFieldChange,
}) => {
    const handleFieldChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        onFieldChange(event.target.value);
    };

    return (
        <Input
            value={value}
            onChange={handleFieldChange}
            className={css.input}
        />
    );
};

export { TaskTitleField };
