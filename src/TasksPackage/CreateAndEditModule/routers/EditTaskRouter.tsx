import * as React from 'react';
import { Route, RouteProps } from 'react-router';

import { EditTaskRoute } from '../routes/EditTaskRoute';
import { CreateAndEditTaskPage } from '../components/CreateAndEditTaskPage';

type Props = RouteProps & {};

export class EditTaskRouter extends React.Component<Props> {
    public static readonly defaultProps: Partial<Props> = {
        exact: true,
        path: EditTaskRoute.url(),
    };

    public render() {
        return <Route {...this.props} component={CreateAndEditTaskPage} />;
    }
}
