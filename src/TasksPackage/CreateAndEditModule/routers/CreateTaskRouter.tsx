import * as React from 'react';
import { Route, RouteProps } from 'react-router';

import { CreateTaskRoute } from '../routes/CreateTaskRoute';
import { CreateAndEditTaskPage } from '../components/CreateAndEditTaskPage';

type Props = RouteProps & {};

export class CreateTaskRouter extends React.Component<Props> {
    public static readonly defaultProps: Partial<Props> = {
        exact: true,
        path: CreateTaskRoute.url(),
    };

    public render() {
        return <Route {...this.props} component={CreateAndEditTaskPage} />;
    }
}
