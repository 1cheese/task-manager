import { parse } from 'date-fns';

import { IRootState } from '../../../CorePackage/MainModule/interfaces/IRootState';
import { ITask } from '../../MainModule/interfaces/ITask';
import { getTaskFieldByFieldAlias } from '../selectors/getTaskFieldByFieldAlias';
import { TaskFieldAlias } from '../enums/TaskFieldAlias';
import { DATE_FORMAT } from '../../../CorePackage/MainModule/constants/DATE_FORMAT';

export function createTaskDataToSend(state: IRootState): Omit<ITask, 'id'> {
    const taskName = getTaskFieldByFieldAlias(TaskFieldAlias.NAME)(state).value;
    const assigneeId = getTaskFieldByFieldAlias(TaskFieldAlias.ASSIGNEE_ID)(
        state,
    ).value;
    const startsAt = getTaskFieldByFieldAlias(TaskFieldAlias.STARTING_DATE)(
        state,
    ).value;
    const dueAt = getTaskFieldByFieldAlias(TaskFieldAlias.DUE_DATE)(state)
        .value;
    const description = getTaskFieldByFieldAlias(TaskFieldAlias.DESCRIPTION)(
        state,
    ).value;

    if (
        taskName === undefined ||
        assigneeId === undefined ||
        startsAt === undefined ||
        dueAt === undefined ||
        description === undefined
    ) {
        throw new Error('Not all fields are filled');
    }

    return {
        name: taskName,
        assigneeId: assigneeId,
        startsAt: String(parse(startsAt, DATE_FORMAT, new Date()).getTime()),
        dueAt: String(parse(dueAt, DATE_FORMAT, new Date()).getTime()),
        description: description,
    };
}
