import * as React from 'react';
import { ComponentType } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { IRootState } from '../../../CorePackage/MainModule/interfaces/IRootState';
import { TaskFieldAlias } from '../enums/TaskFieldAlias';
import { getTaskFieldByFieldAlias } from '../selectors/getTaskFieldByFieldAlias';
import { SetTaskFieldByAliasAction } from '../actions/SetTaskFieldByAliasAction';
import { ITaskFieldProps } from '../interfaces/ITaskFieldProps';
import { ITaskField } from '../interfaces/ITaskField';

export const renderTaskFieldWithProps = (
    TaskFieldComponent: ComponentType<ITaskFieldProps>,
    fieldAlias: TaskFieldAlias,
): JSX.Element => {
    const fieldProps = useSelector<IRootState, ITaskField>(state =>
        getTaskFieldByFieldAlias(fieldAlias)(state),
    );
    const dispatch = useDispatch();

    return (
        <TaskFieldComponent
            onFieldChange={value => {
                dispatch(
                    new SetTaskFieldByAliasAction({
                        value: value,
                        fieldAlias: fieldAlias,
                    }),
                );
            }}
            {...fieldProps}
        />
    );
};
