import { ICreateAndEditTaskState } from '../interfaces/ICreateAndEditTaskState';
import { TaskFieldAlias } from '../enums/TaskFieldAlias';

export const createAndEditTaskDefaultState: ICreateAndEditTaskState = {
    isFetching: false,
    fields: {
        [TaskFieldAlias.NAME]: {},
        [TaskFieldAlias.STARTING_DATE]: {
            label: 'Starting date',
        },
        [TaskFieldAlias.STARTING_TIME]: {
            label: 'Starting time',
        },
        [TaskFieldAlias.DUE_DATE]: {
            label: 'Due date',
        },
        [TaskFieldAlias.DUE_TIME]: {
            label: 'Due time',
        },
        [TaskFieldAlias.DESCRIPTION]: {
            label: 'Description',
        },
        [TaskFieldAlias.ASSIGNEE_ID]: {
            label: 'Assignee',
        },
    },
};
