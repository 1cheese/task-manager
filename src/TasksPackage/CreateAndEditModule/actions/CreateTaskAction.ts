import { Action } from 'redux';
import { AsyncAction } from 'redux-promise-middleware';

import { TasksEndpoint } from '../../../CorePackage/ApiModule/endpoints/TasksEndpoint';
import { ITask } from '../../MainModule/interfaces/ITask';

export class CreateTaskAction implements AsyncAction {
    public static readonly type = 'CREATE_TASK';

    public readonly type = CreateTaskAction.type;
    public readonly payload = TasksEndpoint.createTask(this.formData);

    public constructor(private readonly formData: ITask) {}
}

export class CreateTaskPendingAction implements Action {
    public static readonly type = 'CREATE_TASK_PENDING';

    public readonly type = CreateTaskPendingAction.type;
}

export class CreateTaskRejectedAction implements Action {
    public static readonly type = 'CREATE_TASK_REJECTED';

    public readonly type = CreateTaskRejectedAction.type;
}

export class CreateTaskFulfilledAction implements Action {
    public static readonly type = 'CREATE_TASK_FULFILLED';

    public readonly type = CreateTaskFulfilledAction.type;
    public readonly payload?: Promise<ITask>;
}
