import { Action } from 'redux';

export class ClearCreateAndEditTaskStateAction implements Action {
    public static readonly type = 'CLEAR_CREATE_AND_EDIT_TASK_STATE';

    public readonly type = ClearCreateAndEditTaskStateAction.type;

    public constructor() {}
}
