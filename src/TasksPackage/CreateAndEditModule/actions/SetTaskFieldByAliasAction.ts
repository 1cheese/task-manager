import { Action } from 'redux';

import { TaskFieldAlias } from '../enums/TaskFieldAlias';
import { TaskFieldValue } from '../types/TaskFieldValue';

export class SetTaskFieldByAliasAction implements Action {
    public static readonly type = 'SET_TASK_FIELD_BY_ALIAS';

    public readonly type = SetTaskFieldByAliasAction.type;

    public constructor(
        public readonly payload: {
            fieldAlias: TaskFieldAlias;
            value: TaskFieldValue;
        },
    ) {}
}
