import { Action } from 'redux';
import { AsyncAction } from 'redux-promise-middleware';

import { TasksEndpoint } from '../../../CorePackage/ApiModule/endpoints/TasksEndpoint';
import { ITask } from '../../MainModule/interfaces/ITask';

export class EditTaskAction implements AsyncAction {
    public static readonly type = 'EDIT_TASK';

    public readonly type = EditTaskAction.type;
    public readonly payload = TasksEndpoint.editTask(this.formData);

    public constructor(private readonly formData: ITask) {}
}

export class EditTaskPendingAction implements Action {
    public static readonly type = 'EDIT_TASK_PENDING';

    public readonly type = EditTaskPendingAction.type;
}

export class EditTaskRejectedAction implements Action {
    public static readonly type = 'EDIT_TASK_REJECTED';

    public readonly type = EditTaskRejectedAction.type;
}

export class EditTaskFulfilledAction implements Action {
    public static readonly type = 'EDIT_TASK_FULFILLED';

    public readonly type = EditTaskFulfilledAction.type;
    public readonly payload?: Promise<ITask>;
}
