import { Action } from 'redux';
import { AsyncAction } from 'redux-promise-middleware';

import { TasksEndpoint } from '../../../CorePackage/ApiModule/endpoints/TasksEndpoint';
import { ITask } from '../../MainModule/interfaces/ITask';
import { IUser } from '../../../CorePackage/MainModule/interfaces/IUser';

export class FetchTaskByIdAction implements AsyncAction {
    public static readonly type = 'FETCH_TASK_BY_ID';

    public readonly type = FetchTaskByIdAction.type;
    public readonly payload = TasksEndpoint.getTaskById(this.taskId);

    public constructor(private readonly taskId: ITask['id']) {}
}

export class FetchTaskByIdPendingAction implements Action {
    public static readonly type = 'FETCH_TASK_BY_ID_PENDING';

    public readonly type = FetchTaskByIdPendingAction.type;
}

export class FetchTaskByIdRejectedAction implements Action {
    public static readonly type = 'FETCH_TASK_BY_ID_REJECTED';

    public readonly type = FetchTaskByIdRejectedAction.type;
}

export class FetchTaskByIdFulfilledAction implements Action {
    public static readonly type = 'FETCH_TASK_BY_ID_FULFILLED';

    public readonly type = FetchTaskByIdFulfilledAction.type;
    public readonly payload: ITask = {
        id: '',
        name: '',
        assigneeId: '',
        startsAt: '',
        dueAt: '',
        description: '',
    };
}
