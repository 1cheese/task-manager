import { TaskFieldAlias } from '../enums/TaskFieldAlias';
import { ITaskField } from './ITaskField';

export interface ICreateAndEditTaskState {
    isFetching: boolean;
    fields: Record<TaskFieldAlias, ITaskField>;
}
