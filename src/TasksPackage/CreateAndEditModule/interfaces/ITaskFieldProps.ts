import { ITaskField } from './ITaskField';
import { TaskFieldValue } from '../types/TaskFieldValue';

export interface ITaskFieldProps extends ITaskField {
    onFieldChange: (value: TaskFieldValue) => void;
}
