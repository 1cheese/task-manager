import { TaskFieldValue } from '../types/TaskFieldValue';

export interface ITaskField {
    value?: TaskFieldValue;
    label?: string;
}
