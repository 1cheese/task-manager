import { Reducer } from 'redux';
import { format } from 'date-fns';

import {
    FetchTaskByIdAction,
    FetchTaskByIdFulfilledAction,
    FetchTaskByIdPendingAction,
    FetchTaskByIdRejectedAction,
} from '../actions/FetchTaskByIdAction';
import { ICreateAndEditTaskState } from '../interfaces/ICreateAndEditTaskState';
import { createAndEditTaskDefaultState } from '../utils/createAndEditTaskDefaultState';
import { ITask } from '../../MainModule/interfaces/ITask';
import { TaskFieldAlias } from '../enums/TaskFieldAlias';
import { DATE_FORMAT } from '../../../CorePackage/MainModule/constants/DATE_FORMAT';

type TAction =
    | FetchTaskByIdAction
    | FetchTaskByIdPendingAction
    | FetchTaskByIdRejectedAction
    | FetchTaskByIdFulfilledAction;

export const fetchTaskByIdReducer: Reducer<ICreateAndEditTaskState, TAction> = (
    state = createAndEditTaskDefaultState,
    action,
): ICreateAndEditTaskState => {
    switch (action.type) {
        case FetchTaskByIdPendingAction.type:
            return {
                ...state,
                isFetching: true,
            };

        case FetchTaskByIdRejectedAction.type:
            return {
                ...state,
                isFetching: false,
            };

        case FetchTaskByIdFulfilledAction.type: {
            const newFieldsArray = Object.entries(state.fields).map(
                ([fieldAlias, fieldData]) => {
                    const key = fieldAlias as keyof ITask;

                    return [
                        fieldAlias,
                        {
                            ...fieldData,
                            value:
                                action.payload[key] === undefined
                                    ? fieldData.value
                                    : fieldAlias ===
                                          TaskFieldAlias.STARTING_DATE ||
                                      fieldAlias === TaskFieldAlias.DUE_DATE
                                    ? format(
                                          Number(action.payload[key]),
                                          DATE_FORMAT,
                                      )
                                    : action.payload[key],
                        },
                    ];
                },
            );

            return {
                ...state,
                isFetching: false,
                fields: Object.fromEntries(newFieldsArray),
            };
        }

        default:
            return state;
    }
};
