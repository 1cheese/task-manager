import { Reducer } from 'redux';

import { ICreateAndEditTaskState } from '../interfaces/ICreateAndEditTaskState';
import { createAndEditTaskDefaultState } from '../utils/createAndEditTaskDefaultState';
import { SetTaskFieldByAliasAction } from '../actions/SetTaskFieldByAliasAction';
import { ClearCreateAndEditTaskStateAction } from '../actions/ClearCreateAndEditTaskStateAction';

type TAction = SetTaskFieldByAliasAction | ClearCreateAndEditTaskStateAction;

export const taskBasicReducer: Reducer<ICreateAndEditTaskState, TAction> = (
    state = createAndEditTaskDefaultState,
    action,
): ICreateAndEditTaskState => {
    switch (action.type) {
        case SetTaskFieldByAliasAction.type:
            return {
                ...state,
                fields: {
                    ...state.fields,
                    [action.payload.fieldAlias]: {
                        ...state.fields[action.payload.fieldAlias],
                        value: action.payload.value,
                    },
                },
            };

        case ClearCreateAndEditTaskStateAction.type: {
            return createAndEditTaskDefaultState;
        }

        default:
            return state;
    }
};
