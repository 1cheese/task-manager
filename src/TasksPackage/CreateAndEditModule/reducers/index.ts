import reduceReducers from 'reduce-reducers';

import { createTaskReducer } from './createTaskReducer';
import { ICreateAndEditTaskState } from '../interfaces/ICreateAndEditTaskState';
import { createAndEditTaskDefaultState } from '../utils/createAndEditTaskDefaultState';
import { fetchTaskByIdReducer } from './fetchTaskByIdReducer';
import { taskBasicReducer } from './taskBasicReducer';
import { editTaskReducer } from './editTaskReducer';

export const createAndEditTaskReducer = reduceReducers<
    ICreateAndEditTaskState,
    any
>(
    createAndEditTaskDefaultState ?? null,
    createTaskReducer,
    fetchTaskByIdReducer,
    taskBasicReducer,
    editTaskReducer,
);
