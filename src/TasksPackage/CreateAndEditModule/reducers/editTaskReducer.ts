import { Reducer } from 'redux';

import { createAndEditTaskDefaultState } from '../utils/createAndEditTaskDefaultState';
import {
    EditTaskAction,
    EditTaskFulfilledAction,
    EditTaskPendingAction,
    EditTaskRejectedAction,
} from '../actions/EditTaskAction';
import { ICreateAndEditTaskState } from '../interfaces/ICreateAndEditTaskState';

type TAction =
    | EditTaskAction
    | EditTaskPendingAction
    | EditTaskRejectedAction
    | EditTaskFulfilledAction;

export const editTaskReducer: Reducer<ICreateAndEditTaskState, TAction> = (
    state = createAndEditTaskDefaultState,
    action,
): ICreateAndEditTaskState => {
    switch (action.type) {
        case EditTaskPendingAction.type:
            return {
                ...state,
                isFetching: true,
            };

        case EditTaskRejectedAction.type:
        case EditTaskFulfilledAction.type:
            return {
                ...state,
                isFetching: false,
            };

        default:
            return state;
    }
};
