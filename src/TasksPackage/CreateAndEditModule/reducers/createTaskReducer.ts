import { Reducer } from 'redux';

import { ICreateAndEditTaskState } from '../interfaces/ICreateAndEditTaskState';
import { createAndEditTaskDefaultState } from '../utils/createAndEditTaskDefaultState';
import {
    CreateTaskAction,
    CreateTaskFulfilledAction,
    CreateTaskPendingAction,
    CreateTaskRejectedAction,
} from '../actions/CreateTaskAction';

type TAction =
    | CreateTaskAction
    | CreateTaskPendingAction
    | CreateTaskRejectedAction
    | CreateTaskFulfilledAction;

export const createTaskReducer: Reducer<ICreateAndEditTaskState, TAction> = (
    state = createAndEditTaskDefaultState,
    action,
): ICreateAndEditTaskState => {
    switch (action.type) {
        case CreateTaskPendingAction.type:
            return {
                ...state,
                isFetching: true,
            };

        case CreateTaskRejectedAction.type:
        case CreateTaskFulfilledAction.type:
            return {
                ...state,
                isFetching: false,
            };

        default:
            return state;
    }
};
