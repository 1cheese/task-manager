import { nanoid } from 'nanoid';
import { ThunkAction } from '../../../../types/redux-thunk-promise';

import { IRootState } from '../../../CorePackage/MainModule/interfaces/IRootState';
import { CreateTaskAction } from '../actions/CreateTaskAction';
import { ClearCreateAndEditTaskStateAction } from '../actions/ClearCreateAndEditTaskStateAction';
import { browserHistory } from '../../../CorePackage/MainModule/utils/browserHistory';
import { TasksListRoute } from '../../TasksListModule/routes/TasksListRoute';
import { createTaskDataToSend } from '../utils/createTaskDataToSend';
import { ITask } from '../../MainModule/interfaces/ITask';

export function createTask(): ThunkAction<void, IRootState> {
    return async (dispatch, getState) => {
        const state = getState();
        const taskId = nanoid(5);
        const newTask: ITask = {
            id: taskId,
            ...createTaskDataToSend(state),
        };

        const action = await dispatch(new CreateTaskAction(newTask));

        if (action.value.id !== undefined && action.value.id === taskId) {
            dispatch(new ClearCreateAndEditTaskStateAction());

            browserHistory.push(TasksListRoute.url());
        }
    };
}
