import { ThunkAction } from '../../../../types/redux-thunk-promise';
import { IRootState } from '../../../CorePackage/MainModule/interfaces/IRootState';
import { ClearCreateAndEditTaskStateAction } from '../actions/ClearCreateAndEditTaskStateAction';
import { browserHistory } from '../../../CorePackage/MainModule/utils/browserHistory';
import { TasksListRoute } from '../../TasksListModule/routes/TasksListRoute';
import { createTaskDataToSend } from '../utils/createTaskDataToSend';
import { ITask } from '../../MainModule/interfaces/ITask';
import { EditTaskAction } from '../actions/EditTaskAction';

export function editTask(taskId: ITask['id']): ThunkAction<void, IRootState> {
    return async (dispatch, getState) => {
        const state = getState();
        const editedTask: ITask = {
            id: taskId,
            ...createTaskDataToSend(state),
        };

        const action = await dispatch(new EditTaskAction(editedTask));

        if (action.value.id !== undefined && action.value.id === taskId) {
            dispatch(new ClearCreateAndEditTaskStateAction());

            browserHistory.push(TasksListRoute.url());
        }
    };
}
