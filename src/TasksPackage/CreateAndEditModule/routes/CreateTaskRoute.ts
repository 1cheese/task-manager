import { AbstractRoute, route } from '../../../CorePackage/RouteModule';

@route()
export class CreateTaskRoute extends AbstractRoute {
    public static url() {
        return '/tasks/create';
    }
}
