import { AbstractRoute, route } from '../../../CorePackage/RouteModule';
import { ITask } from '../../MainModule/interfaces/ITask';

type EditTaskRouteParams = {
    id: ITask['id'];
};

@route<EditTaskRouteParams>()
export class EditTaskRoute extends AbstractRoute<EditTaskRouteParams> {
    public static url({ id }: EditTaskRouteParams = { id: ':id(\\w+)' }) {
        return `/tasks/edit/${id}`;
    }
}
