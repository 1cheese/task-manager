import * as React from 'react';
import * as ReactDOM from 'react-dom';

import './index.scss';
import 'bootstrap/dist/css/bootstrap.min.css';
import { App } from './CorePackage/MainModule/components/App';

const container = document.querySelector('#app');

ReactDOM.render(
    React.createElement(React.StrictMode, null, React.createElement(App, null)),
    container,
);
