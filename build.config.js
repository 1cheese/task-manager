const path = require('path');

class Config {
    constructor() {
        this.distDirectory = 'dist';
        this.jsonServer = {
            port: 9000,
            path: path.resolve(__dirname, 'json-server', 'db.json'),
        };
        this.apiGateways = {
            local: `http://localhost:${this.jsonServer.port}`,
        };
    }

    getApiUrl(isProduction) {
        return this.apiGateways.local;
    }
}

module.exports = new Config();
